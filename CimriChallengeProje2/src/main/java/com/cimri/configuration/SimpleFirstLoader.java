package com.cimri.configuration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.cimri.repositories.ProductPriceDateRepository;
import com.cimri.repositories.ProductRepository;


@Component
@SuppressWarnings("unused")
public class SimpleFirstLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	private Logger log = Logger.getLogger(SimpleFirstLoader.class);
	  
    private ProductRepository productRepository;
	private ProductPriceDateRepository productPriceDateRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    
    @Autowired
    public void setProductPriceDateRepository(ProductPriceDateRepository productPriceDateRepository) {
        this.productPriceDateRepository = productPriceDateRepository;
    }

 
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    }
}
