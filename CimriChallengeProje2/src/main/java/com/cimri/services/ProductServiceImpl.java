package com.cimri.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cimri.models.Product;
import com.cimri.repositories.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
   
	private ProductRepository productRepository;
 
    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Iterable<Product> listAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.findOne(id);
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }
    
    @Override
    public  Iterable<Product>  getProductsByTitle(String title) {
        return productRepository.findByTitleReturnStream(title);
        }

}
