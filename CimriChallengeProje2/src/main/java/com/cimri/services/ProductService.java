package com.cimri.services;

import com.cimri.models.Product;

public interface ProductService {
    Iterable<Product> listAllProducts();

    Product getProductById(Integer id);

    Product saveProduct(Product product);
    
    Iterable<Product> getProductsByTitle(String title);
}