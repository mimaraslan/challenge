package com.cimri.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cimri.models.ProductPriceDate;
import com.cimri.repositories.ProductPriceDateRepository;

@Service
@Transactional
public class ProductPriceDateServiceImpl implements ProductPriceDateService {
   
	private ProductPriceDateRepository productPriceDateRepository;

    @Autowired
    public void setProductPriceDateRepository(ProductPriceDateRepository productPriceDateRepository) {
        this.productPriceDateRepository = productPriceDateRepository;
    }

    @Override
    public Iterable<ProductPriceDate> listAllProductPriceDates() {
        return productPriceDateRepository.findAll();
    }

    @Override
    public ProductPriceDate getProductPriceDateById(Integer id) {
        return productPriceDateRepository.findOne(id);
    }

    @Override
    public ProductPriceDate saveProductPriceDate(ProductPriceDate productPriceDate) {
        return productPriceDateRepository.save(productPriceDate);
    }
      
    @Override
    public  Iterable<ProductPriceDate>  getProductPriceDatesByTitle(String title) {
        return productPriceDateRepository.findByTitleReturnStream(title);
        }
    
    @Override
    public  Iterable<ProductPriceDate>  getProductPriceDatesBySiteId(Integer id) {
        return productPriceDateRepository.findBySiteIdReturnStream(id);
        } 

}
