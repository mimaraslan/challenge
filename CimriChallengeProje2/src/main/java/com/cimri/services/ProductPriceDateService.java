package com.cimri.services;

import com.cimri.models.ProductPriceDate;

public interface ProductPriceDateService {
    Iterable<ProductPriceDate> listAllProductPriceDates();

    ProductPriceDate getProductPriceDateById(Integer id);

    ProductPriceDate saveProductPriceDate(ProductPriceDate productPriceDate);
    
    Iterable<ProductPriceDate> getProductPriceDatesByTitle(String title);

	Iterable<ProductPriceDate> getProductPriceDatesBySiteId(Integer id);
}