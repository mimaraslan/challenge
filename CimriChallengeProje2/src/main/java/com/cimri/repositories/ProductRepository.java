package com.cimri.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cimri.models.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{
	
	/*
	@Query("SELECT t FROM Product t WHERE " +
            "LOWER(t.title) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR " +
            "LOWER(t.category) LIKE LOWER(CONCAT('%',:searchTerm, '%'))")
	Iterable<Product> findByTitleOrCategoryIgnoreCase(@Param("searchTerm") String searchTerm);
	*/
	
	 @Query("SELECT p FROM Product p WHERE LOWER(p.title) LIKE LOWER(CONCAT(:title, '%')) ")
	  Iterable<Product> findByTitleReturnStream(@Param("title") String title);

}