package com.cimri.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cimri.models.ProductPriceDate;

public interface ProductPriceDateRepository extends CrudRepository<ProductPriceDate, Integer>{

	 @Query("SELECT p FROM ProductPriceDate p WHERE LOWER(p.title) LIKE LOWER(CONCAT(:title, '%')) ")
	 Iterable<ProductPriceDate> findByTitleReturnStream(@Param("title") String title);

	 @Query("SELECT p FROM ProductPriceDate p WHERE p.id =:id ORDER BY p.pricedate ASC)")
	 Iterable<ProductPriceDate> findBySiteIdReturnStream(@Param("id") Integer id);

}