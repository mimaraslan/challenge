package com.cimri.controllers;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cimri.models.Product;
import com.cimri.services.ProductService;

@Controller
@SuppressWarnings("unused")
public class ProductController {

	private static final Logger logger = Logger.getLogger(ProductController.class);

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    } 
    
    public ProductController() {
		System.out.println("ProductController()");
	}
    
 // http://localhost:8080/products/json
   	@RequestMapping(value = "/products/json",	
 					method = RequestMethod.GET, 
 					headers = "Accept=application/json; charset=UTF-8",  
 					produces = "application/json; charset=UTF-8")
 	public ResponseEntity<Iterable<Product>> getAllProductsListAllJson() {
   		
   		Iterable<Product> products = productService.listAllProducts();

 		if (products != null) {
 			return new ResponseEntity<Iterable<Product>>(products, HttpStatus.OK);	
 		}else {
 			return new ResponseEntity<Iterable<Product>>(HttpStatus.NOT_FOUND);
 		}
 	}	
 
   	
   	
    // http://localhost:8080/product/json/1
   	@RequestMapping(value = "/product/json/{id}",	
 					method = RequestMethod.GET, 
 					headers = "Accept=application/json; charset=UTF-8",  
 					produces = "application/json; charset=UTF-8")
 	public ResponseEntity<Product> getProductByIdJson(@PathVariable ("id") Integer id) {
   		
   		Product product = productService.getProductById(id);

 		if (product != null) {
 			return new ResponseEntity<Product>(product, HttpStatus.OK);	
 		}else {
 			return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
 		}
 	}	
   	
   	
   

    
    @RequestMapping(value = "/grafik", method = RequestMethod.GET)
    public String grafik(){
        return "grafik";
    }
    
 // http://localhost:8080/products
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("products", productService.listAllProducts());
        System.out.println("Returning products:");
        return "products";
    }

    
    //  http://localhost:8080/product/id
    @RequestMapping("product/{id}")
    public String showProduct(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.getProductById(id));
        return "productshow";
    }

    @RequestMapping("product/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.getProductById(id));
        return "productform";
    }

    //  http://localhost:8080/product/new
    @RequestMapping("product/new")
    public String newProduct(Model model){
        model.addAttribute("product", new Product());
        return "productform";
    }

    //  http://localhost:8080/product
    @RequestMapping(value = "product", method = RequestMethod.POST)
    public String saveProduct(Product product){
      	Product productInfo = productService.saveProduct(product);
      	
      	 //  http://localhost:8080/product/id
        return "redirect:/product/" + productInfo.getIdtable();
    }

	@RequestMapping(value= "/products/search", method = RequestMethod.GET)
	public String searchProduct(@RequestParam("title") String title, Model model) {
		//logger.info("Search product name: " + searchName);
		//List<Product> product_list = productService.findByNameProducts(searchName);
		//return new ModelAndView("product_list", "product_list", product_list);
		
	        model.addAttribute("products", productService.getProductsByTitle(title));
	        
	       // model.addAttribute("products", productService.listAllProducts());
	        System.out.println("Returning products:" + title);
	        return "products";
	}
	
	
}
