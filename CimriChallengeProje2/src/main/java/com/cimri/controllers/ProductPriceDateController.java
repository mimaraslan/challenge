package com.cimri.controllers;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.cimri.models.ProductPriceDate;
import com.cimri.services.ProductPriceDateService;

@Controller
@SuppressWarnings("unused")
public class ProductPriceDateController {
    
	private static final Logger logger = Logger.getLogger(ProductPriceDateController.class);

	private ProductPriceDateService productPriceDateService;

    @Autowired
    public void setProductPriceDateService(ProductPriceDateService productPriceDateService) {
        this.productPriceDateService = productPriceDateService;
    }  
    
	public ProductPriceDateController() {
		System.out.println("ProductPriceDateRestController()");
	}

	// http://localhost:8080/productpricedates/json
  	@RequestMapping(value = "productpricedates/json",	
					method = RequestMethod.GET, 
					headers = "Accept=application/json; charset=UTF-8",  
					produces = "application/json; charset=UTF-8")
	public ResponseEntity<Iterable<ProductPriceDate>> getAllProductPriceDatesListAllJson() {
  		
		Iterable<ProductPriceDate> productPriceDates = productPriceDateService.listAllProductPriceDates();

		if (productPriceDates != null) {
			return new ResponseEntity<Iterable<ProductPriceDate>>(productPriceDates, HttpStatus.OK);	
		}else {
			return new ResponseEntity<Iterable<ProductPriceDate>>(HttpStatus.NOT_FOUND);
		}
	}		
	
  	
  	
  	
    // http://localhost:8080/productpricedate/json/idtable/1
   	@RequestMapping(value = "productpricedate/json/idtable/{id}",	
 					method = RequestMethod.GET, 
 					headers = "Accept=application/json; charset=UTF-8",  
 					produces = "application/json; charset=UTF-8")
 	public ResponseEntity<ProductPriceDate> getProductByIdJson(@PathVariable ("id") Integer id) {
   		
   		ProductPriceDate productPriceDate = productPriceDateService.getProductPriceDateById(id);

 		if (productPriceDate != null) {
 			return new ResponseEntity<ProductPriceDate>(productPriceDate, HttpStatus.OK);	
 		}else {
 			return new ResponseEntity<ProductPriceDate>(HttpStatus.NOT_FOUND);
 		}
 	}	
   	
  
    // http://localhost:8080/productpricedate/json/id/864761
   	@RequestMapping(value = "productpricedate/json/id/{id}",	
 					method = RequestMethod.GET, 
 					headers = "Accept=application/json; charset=UTF-8",  
 					produces = "application/json; charset=UTF-8")
 	public ResponseEntity<Iterable<ProductPriceDate>> getProductBySiteIdJson(@PathVariable ("id") Integer id) {
  		
		Iterable<ProductPriceDate> productPriceDates = productPriceDateService.getProductPriceDatesBySiteId(id);

		if (productPriceDates != null) {
			return new ResponseEntity<Iterable<ProductPriceDate>>(productPriceDates, HttpStatus.OK);	
		}else {
			return new ResponseEntity<Iterable<ProductPriceDate>>(HttpStatus.NOT_FOUND);
		}
	}			
  	

    // http://localhost:8080/productpricedates
    @RequestMapping(value = "productpricedates", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("productpricedates", productPriceDateService.listAllProductPriceDates());
        System.out.println("Returning productPriceDateServices:");
        return "productpricedates";
    }
   
    
    @RequestMapping("productpricedate/{id}")
    public String showProduct(@PathVariable Integer id, Model model){
        model.addAttribute("productpricedate", productPriceDateService.getProductPriceDateById(id));
        return "productpricedateshow";
    }

    @RequestMapping("productpricedate/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("productpricedate", productPriceDateService.getProductPriceDateById(id));
        return "productpricedateform";
    }

    @RequestMapping("productpricedate/new")
    public String newProduct(Model model){
        model.addAttribute("productpricedate", new ProductPriceDate());
        return "productpricedateform";
    }

    @RequestMapping(value = "productpricedate", method = RequestMethod.POST)
    public String saveProduct(ProductPriceDate productpricedate){
    	    ProductPriceDate productpricedateInfo = productPriceDateService.saveProductPriceDate(productpricedate);
        return "redirect:/productpricedate/" + productpricedateInfo.getIdtable();
    }

	@RequestMapping(value= "/productpricedates/search", method = RequestMethod.GET)
	public String searchProductPriceDate(@RequestParam("title") String title, Model model) {
		//logger.info("Search product name: " + searchName);
		//List<Product> product_list = productService.findByNameProducts(searchName);
		//return new ModelAndView("product_list", "product_list", product_list);
		
	        model.addAttribute("productpricedates", productPriceDateService.getProductPriceDatesByTitle(title));
	        
	       // model.addAttribute("products", productService.listAllProducts());
	        System.out.println("Returning productpricedates:" + title);
	        return "productpricedates";
	}
	
}
