https://bitbucket.org/cimri/challenge

## Cimri Challenge Proje 2

## 1.Projeyi anlatan video bu adrestedir.  [https://yadi.sk/i/HRLqNorB3RoLTJ](https://yadi.sk/i/HRLqNorB3RoLTJ)

## 2. Projeyi anlatan video bu adrestedir.  [https://yadi.sk/i/S58e2L7p3RoUSV](https://yadi.sk/i/S58e2L7p3RoUSV)

#Bu uygulamayı çalıştırırken MySQL açık olmalı.

#MySQL tablolarından veriler Spring template ile web formalarına çekiliyor.
#Projede MVC tasarlandı ve çok katmanlı mimariye göre kodlandı.
#Başka tarayıcılar arasında Javascript sorunları yaşamamak için projenin CORS ayarları yapıldı.
#MySQL tablolar üzerinde CRUD işlemleri yapılıyor.
#Tablolardaki veriler JSON'lara çevriliyor. 
#Bu projede JPA ve Hibernate de kullanıldı.
#Seçilen ürünün fiyat değişim grafiği oluşturuldu.