https://bitbucket.org/cimri/challenge

## Cimri Challenge Proje 1

## 1.Projeyi anlatan video bu adrestedir.  [https://yadi.sk/i/HRLqNorB3RoLTJ](https://yadi.sk/i/HRLqNorB3RoLTJ)

## 2. Projeyi anlatan video bu adrestedir. [https://yadi.sk/i/S58e2L7p3RoUSV](https://yadi.sk/i/S58e2L7p3RoUSV)

#Bu uygulamayı çalıştırırken Cassandra ve MySQL açık olmalıdır yoksa hata alırsınız.
#Url'deki XML dosyaları alınıp içleri okunuyor ve Java ile pars ediliyor.
#Eğer XML'lerdeki veriler sabitse o zaman işlerin daha hızlanması için file olarak kaydedip öyle de parse edebiliriz. Ben her ikisini de yaptım.
#Parse edilen verileri Spring Batch ile MySQL veritabanına aktardık. 
#MySQL veritabanı oluşturuluyor.
#MySQL tablolar oluşturuluyor.
#Cassandra veritabanı oluşturuluyor.
#Cassandra tablolar oluşturuluyor.
#Pars edilen verileri MySQL tablolarına kayıt olarak ekleniyor.
#Pars edilen verileri Cassandra tablolarına kayıt olarak ekleniyor ama berlirli bir yerden sonra hata veriyor. Cassandraya Java içinden batch yapınca belirli bir zaman sonra hata veriyor. O kısımdaki kodları açıklama haline getirdim.
#MySQL tablolarının csv'leri halleri çıkartılıyor.
