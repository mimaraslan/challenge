package com.cimri.repositories;

import java.sql.SQLException;
import java.util.List;

import com.cimri.models.Product;

public interface ProductRepository {

	public void createInCassandraDatabase();
	public void createInCassandraTableProduct();
	public void createInCassandraTableProductPriceDate();
	
	public void createInRdbmsDatabase() throws SQLException;
	public void createInRdbmsTableProduct() throws SQLException; 
	public void createInRdbmsTableProductTitle() throws SQLException;
	public void createInRdbmsTableProductTitleGroup() throws SQLException;
	public void createInRdbmsTableProductPriceDate() throws SQLException;

	public void insertInRdbmsBatchProduct(final List<Product> products);
	
	public void sortInRdbmsTableProductOrderByTitle(int i);
	public void sortInRdbmsTableProductGroupByTitle();
	public void sortInRdbmsTableProductPriceDate();
	
	public void insertInRdbmsProduct(Product product);
	public String findInRdbmsProductNameById(int id);
	public Product findInRdbmsProductById(int id);
	public List<Product> findInRdbmsProductAll();

	public void insertInRdbmsBatchProductPriceDate();	
	public void insertInRdbmsBatchSQL(final String sql);
	public void copyPoductCsvToCassandra();
	public void copyPoductPriceDateCsvToCassandra();

}