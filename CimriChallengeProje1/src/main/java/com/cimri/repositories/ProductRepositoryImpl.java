package com.cimri.repositories;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.cimri.models.Product;
import com.cimri.models.ProductPriceDate;
import com.cimri.services.ProjectProperties;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class ProductRepositoryImpl implements ProductRepository{
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	//PROGRAM ICINDE DEGISEN ADRES GIBI BILGILERI DISARIDAN DEGISTIREBILMEK ICIN DAHA PRATIK HALE GETIRDIK.
	ProjectProperties projectProperties = new ProjectProperties();
 
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void createInCassandraDatabase() {	
		Cluster cluster = Cluster.builder().addContactPoint("localhost").build(); 
		Session session = cluster.connect();
		
		session.execute("DROP KEYSPACE IF EXISTS cimridb;");
		session.execute("CREATE KEYSPACE cimridb WITH replication = {'class':'SimpleStrategy', 'replication_factor':2};");
	}	

public void createInCassandraTableProduct() {
	 Cluster cluster = Cluster.builder().addContactPoint("localhost").build(); 
	 Session session = cluster.connect("cimridb");
	
      String query = "DROP TABLE IF EXISTS cimridb.product;" ;
      session.execute(query);
      System.out.println("Product table dropped in Cassandra.");
    
      																				     // list<text>
      query = "CREATE TABLE IF NOT EXISTS cimridb.product (idtable int PRIMARY KEY, id int, prices text, dates text, title text, brand text, category text, url text );" ;
      session.execute(query);
      System.out.println("Product table created in Cassandra.");
}

public void createInCassandraTableProductPriceDate() {	 
	Cluster cluster = Cluster.builder().addContactPoint("localhost").build(); 
	Session session = cluster.connect("cimridb");
	
	String query = "DROP TABLE IF EXISTS cimridb.product_price_date;" ;
    session.execute(query);
    System.out.println("ProductPriceDate table dropped in Cassandra.");
    
    query = "CREATE TABLE IF NOT EXISTS cimridb.product_price_date(idtable int PRIMARY KEY, id int, title text, pricedate text, calendardate text, price text, brand text, category text, url text);" ;
    session.execute(query);
    System.out.println("ProductPriceDate table created in Cassandra.");
}

// TODO csv COPY TO  Cassandra
public void copyPoductCsvToCassandra() {	 
	Cluster cluster = Cluster.builder().addContactPoint("localhost").build(); 
	Session session = cluster.connect("cimridb");

//	String query = "COPY cimridb.product (idtable, id, prices, dates, title, brand, category, url) FROM '/Users/lion/Documents/workspace/cassandra/CimriChallengeProje1/data/product.csv' with delimiter =';' and WITH header = true and MAXBATCHSIZE = 5000;" ;
	String query = null;
	try {
		query = "COPY cimridb.product (idtable, id, prices, dates, title, brand, category, url) FROM "+projectProperties.getProperty("product.csv")+" WITH HEADER = TRUE;";
	} catch (IOException e) {
		e.printStackTrace();
	}finally {
		session.execute(query);
	    System.out.println("product CSV copied to Cassandra.");
	}
}

//TODO csv COPY TO  Cassandra
public void copyPoductPriceDateCsvToCassandra() {	 
	Cluster cluster = Cluster.builder().addContactPoint("localhost").build(); 
	Session session = cluster.connect("cimridb");

//	String query = " COPY cimridb.product_price_date (idtable, id, title, pricedate, calendardate, price, brand, category, url) FROM '/Users/lion/Documents/workspace/cassandra/CimriChallengeProje1/data/product_price_date.csv' WITH HEADER = TRUE;";

	String query = null;
	try {
		query = "COPY cimridb.product_price_date (idtable, id, title, pricedate, calendardate, price, brand, category, url) FROM "+projectProperties.getProperty("product_price_date.csv")+" WITH HEADER = TRUE;";
	} catch (IOException e) {
		e.printStackTrace();
	}finally {
		session.execute(query);
	    System.out.println("product_price_date CSV copied to Cassandra.");
	}
}

public void createInRdbmsDatabase() throws SQLException{
		//String	sql = "DROP TABLE IF EXISTS cimridb.product;"; 	
		//jdbcTemplate.execute(sql);
	   //  System.out.println("Cimri database dropped in RDBMS.");

        String url = "jdbc:mysql://localhost:3306/";
        String username = "root";
        String password = "12345678";

 	   String	 sql = "CREATE DATABASE IF NOT EXISTS cimridb DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;";
 	  try (Connection conn = DriverManager.getConnection(url, username, password);
              PreparedStatement stmt = conn.prepareStatement(sql)) {
             stmt.execute();
         } catch (Exception e) {
             e.printStackTrace();
         }
        
	    System.out.println("Cimri database created in RDBMS.");   
}	


public void createInRdbmsTableProduct() throws SQLException{
	jdbcTemplate = new JdbcTemplate(dataSource);
		String	sql = "DROP TABLE IF EXISTS cimridb.product;"; 	
		jdbcTemplate.execute(sql);
	     System.out.println("Product table dropped in RDBMS.");

			 sql = "CREATE TABLE IF NOT EXISTS cimridb.product (" + 
				"  idtable INT NOT NULL AUTO_INCREMENT," + 
				"  id INT NULL," + 
				"  prices LONGTEXT NULL," + 
				"  dates LONGTEXT NULL," + 
				"  title LONGTEXT NULL," + 
				"  brand LONGTEXT NULL," + 
				"  category LONGTEXT NULL," + 
				"  url LONGTEXT NULL," + 
				"  PRIMARY KEY (idtable));";
		jdbcTemplate.execute(sql);
	    System.out.println("Product table created in RDBMS.");   
}	

public void createInRdbmsTableProductTitle() throws SQLException{
	jdbcTemplate = new JdbcTemplate(dataSource);
	    String sql = "DROP TABLE IF EXISTS cimridb.product_title;"; 	
		jdbcTemplate.execute(sql);
	     System.out.println("ProductTitle Table dropped in RDBMS.");
	     
			 sql = "CREATE TABLE IF NOT EXISTS cimridb.product_title (" + 
				"  idtable INT NOT NULL AUTO_INCREMENT," + 
				"  id INT NULL," + 
				"  prices LONGTEXT NULL," + 
				"  dates LONGTEXT NULL," + 
				"  title LONGTEXT NULL," + 
				"  brand LONGTEXT NULL," + 
				"  category LONGTEXT NULL," + 
				"  url LONGTEXT NULL," + 
				"  PRIMARY KEY (idtable));";
		jdbcTemplate.execute(sql);	  
	    System.out.println("ProductTitle table created in RDBMS.");   
}	

public void createInRdbmsTableProductTitleGroup() throws SQLException{		
	jdbcTemplate = new JdbcTemplate(dataSource);
	    String sql = "DROP TABLE IF EXISTS cimridb.product_title_group;"; 	
		jdbcTemplate.execute(sql);
	    System.out.println("ProductTitleGroup table dropped in RDBMS.");   

		 sql = "CREATE TABLE IF NOT EXISTS cimridb.product_title_group (" + 
					"  idtable INT NOT NULL AUTO_INCREMENT," + 
					"  id INT NULL," + 
					"  total INT NULL," + 
					"  title LONGTEXT NULL," + 		
					"  PRIMARY KEY (idtable));";
		jdbcTemplate.execute(sql);	
	    System.out.println("ProductTitleGroup table created in RDBMS.");   
}	

public void createInRdbmsTableProductPriceDate() throws SQLException{	
	jdbcTemplate = new JdbcTemplate(dataSource);
	    String	sql = "DROP TABLE IF EXISTS cimridb.product_price_date;"; 	
		jdbcTemplate.execute(sql);
	    System.out.println("ProductPriceDate table dropped in RDBMS.");   

	    sql = "CREATE TABLE IF NOT EXISTS cimridb.product_price_date (" + 
				"  idtable INT NOT NULL AUTO_INCREMENT," + 	
				"  id INT NULL," + 
				"  title LONGTEXT NULL," + 
				"  pricedate TEXT NULL," + 
				"  calendardate LONGTEXT NULL," + 				
				"  price FLOAT NOT NULL DEFAULT 0," + 
				"  brand LONGTEXT NULL," + 
				"  category LONGTEXT NULL," + 
				"  url LONGTEXT NULL," + 
				"  PRIMARY KEY (idtable));";
		jdbcTemplate.execute(sql);
	    System.out.println("ProductPriceDate table created in RDBMS.");   
	}	

public void insertInRdbmsBatchProduct(final List<Product> products){
	  String sql = "INSERT INTO product (id, prices, dates, title, brand, category, url) VALUES(?, ?, ?, ?, ?, ?, ?)";
	
	  jdbcTemplate = new JdbcTemplate(dataSource); 
	  jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
	 
		public void setValues(PreparedStatement ps, int i) throws SQLException {
				Product product = products.get(i);
				ps.setLong(1, product.getId());
				ps.setString(2, product.getPrices());
				ps.setString(3, product.getDates());
				ps.setString(4, product.getTitle());
				ps.setString(5, product.getBrand());
				ps.setString(6, product.getCategory());
				ps.setString(7, product.getUrl());
				
				System.out.println(product);
			}
			public int getBatchSize() {
				return products.size();
			}	
	  });
	}


//URUN ISIMLERINE GORE SIRALI
public void sortInRdbmsTableProductOrderByTitle(int i){	
	String sql = null;
	if(i == 0) {
		 sql = "INSERT INTO cimridb.product_title (id, prices, dates, title, brand, category, url) "
					+ "SELECT id, prices, dates, title, brand, category, url "
					+ "FROM cimridb.product "
					+ "ORDER BY title ASC;";
	}else {
		 sql = "INSERT INTO cimridb.product_title (id, prices, dates, title, brand, category, url) "
					+ "SELECT id, prices, dates, title, brand, category, url "
					+ "FROM cimridb.product "
					+ "ORDER BY title DESC;";
	}
	
	jdbcTemplate = new JdbcTemplate(dataSource);
	jdbcTemplate.execute(sql);
}


// URUN ISIMLERINE GORE GRUPLAMALAMALI SIRALI ve ADEDI BELLI 
public void sortInRdbmsTableProductGroupByTitle(){	
  String sql = "INSERT INTO cimridb.product_title_group (id, total, title) " 
			  + "SELECT id, COUNT(title), title " 
			  + 	"FROM cimridb.product " 
			  + "GROUP BY  title, id ;";
    jdbcTemplate = new JdbcTemplate(dataSource);
	jdbcTemplate.execute(sql);
}
	

// URUN TARIHLERE GORE FIYATLAR burada DETAYLI PARCALAMA YOK 
public void sortInRdbmsTableProductPriceDate(){	
	/* sql = "INSERT INTO cimridb.product_price_date (id, title, prices, dates) " + 
			"SELECT id, title, prices, dates " + 
			"FROM cimridb.product;";
			
	jdbcTemplate = new JdbcTemplate(dataSource);
	jdbcTemplate.execute(sql);
	*/
		//sql = "DROP TABLE cimridb.product;";
		//jdbcTemplate.execute(sql);
}


//---------------------------------------------------------------------------------
	
	public void insertInRdbmsProduct(Product product){
		String sql = "INSERT INTO product (id, prices, dates, title, brand, category, url) VALUES (?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql, new Object[] { product.getId(), product.getPrices(), product.getTitle() });
	}
	
	public String findInRdbmsProductNameById(int id){
		String sql = "SELECT name FROM product WHERE id = ?"; 
		jdbcTemplate = new JdbcTemplate(dataSource);
		String name = jdbcTemplate.queryForObject(sql, new Object[] { id }, String.class);
		return name;
	}
	
	public Product findInRdbmsProductById(int id){	 
		String sql = "SELECT * FROM product WHERE id = ?";
		jdbcTemplate = new JdbcTemplate(dataSource);
		Product product = jdbcTemplate.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Product>(Product.class));
		return product;
	}

	public List<Product> findInRdbmsProductAll(){	
		String sql = "SELECT * FROM product";
	 		
		jdbcTemplate = new JdbcTemplate(dataSource);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
	
		List<Product> products = new ArrayList<Product>();
		
		for (Map<?, ?> row : rows) {
			Product product = new Product();
			product.setId(Integer.parseInt(String.valueOf(row.get("id"))));
			product.setPrices((String)row.get("prices"));
			product.setDates((String)row.get("dates"));
			product.setTitle((String)row.get("title"));
			product.setBrand((String)row.get("brand"));
			product.setCategory((String)row.get("category"));
			product.setUrl((String)row.get("url"));
			products.add(product);
		}	 
		return products;
	}


//---------------------------------------------------------------------------------
    
	// TODO insertInCassandraProduct METODU ILE DOGRUDAN CASSANDRAYA EKLEME YAPINCA BELLI BIR ZAMAN SONRA CASSANDRA DONUYOR. 
    @SuppressWarnings("unused")
	private static void insertInCassandraProduct(int counter, Product product) {
	    Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
	    Session session = cluster.connect("cimridb");
	    	    
			  String query = ("INSERT INTO product (idtable, id, prices, dates, title, brand, category, url) "
		                + "VALUES ("+counter+", "+product.getId()+", '"+product.getPrices()+"', '"+product.getDates()+"', '"+product.getTitle()+"', '"+product.getBrand()+"', '"
		                				+product.getCategory()+"', '"+product.getUrl()+"');");

		      session.execute(query);
		      System.out.println(counter+", "+product.getId()+", '"+product.getPrices()+"', '"+product.getDates()+"', '"+product.getTitle()+"', '"+product.getBrand()+"', '"
      				+product.getCategory()+"', '"+product.getUrl());
		      session.closeAsync();
		      cluster.closeAsync();
	}
	
    
    
	// TODO insertInCassandraProductPriceDate METODU ILE DOGRUDAN CASSANDRAYA EKLEME YAPINCA BELLI BIR ZAMAN SONRA CASSANDRA DONUYOR. 
    @SuppressWarnings("unused")
	private static void insertInCassandraProductPriceDate(ProductPriceDate product_price_date) {
    	
    	// TODO BURADAN EKLEME YAPILDIGI ZAMAN 2300 KAYITTAN SONRA CASSANDRA DONUYOR. 
    	if(product_price_date.getIdtable()%2300==0)
    		try{Thread.sleep(500); }catch(InterruptedException e){System.out.println(e);}  
    	
    	     Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
         Session session = cluster.connect("cimridb");
    
        
	 //   System.out.println(product_price_date.getPrice());
	    
			  String query = ("INSERT INTO product_price_date(idtable, id, title, pricedate, calendardate, price, brand, category, url) VALUES ("+product_price_date.getIdtable()+", "+ 
					  	     product_price_date.getId()+", '"+product_price_date.getTitle()+"', '"+
		                     product_price_date.getPricedate()+"', '"+product_price_date.getCalendardate() +"', '"+
		                     product_price_date.getPrice()+"', '"+product_price_date.getBrand()+"', '"+
		                     product_price_date.getCategory()+"', '"+product_price_date.getUrl()+"');");

		      session.execute(query);
		      session.closeAsync();
		      cluster.closeAsync();
		   // System.out.println(product_price_date);
	}
	
    
    
    
    
    public static String convertMilliSecondsToFormattedDate(String milliSeconds){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return new SimpleDateFormat("dd-MM-yyyy hh:mm").format(calendar.getTime());
    }  
    
    private static AtomicInteger atomicIntegerCounter = new AtomicInteger(0);
    
	public void insertInRdbmsBatchPricesDates(final Product product, final List<String> itemsDates, final List<String> itemsPrices){

		int counterIdTable = atomicIntegerCounter.incrementAndGet();
		
		// TODO insertInCassandraProduct METODU ILE DOGRUDAN CASSANDRAYA EKLEME YAPINCA BELLI BIR ZAMAN SONRA CASSANDRA DONUYOR. 
		// insertInCassandraProduct(counterIdTable, product);
    
		  String sql = "INSERT INTO product_price_date (id, title, pricedate, calendardate, price, brand, category, url) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
		 
		  jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
		 
			public void setValues(PreparedStatement ps, int i) throws SQLException {
			//	System.out.println(product.getId()+ " "+ product.getTitle());
				
				ProductPriceDate product_price_date = new ProductPriceDate();
				
				ps.setInt(1, product.getId()); 
				ps.setString(2, product.getTitle());
				
				product_price_date.setIdtable(counterIdTable);
				
				
				 if(itemsDates.get(i)=="" || itemsDates.get(i)==null) {
					 System.out.println(product_price_date.getIdtable() +" NO DATE: "+ itemsDates.get(i));
					 System.out.println(product);
					 
					    ps.setString(3, null);
						ps.setString(4, null );
						
						product_price_date.setPricedate("");
						product_price_date.setCalendardate("");							
				 }else {
						ps.setString(3, itemsDates.get(i));
						ps.setString(4, convertMilliSecondsToFormattedDate(itemsDates.get(i)) );
						
						product_price_date.setPricedate(itemsDates.get(i));
						product_price_date.setCalendardate(convertMilliSecondsToFormattedDate(itemsDates.get(i)));					
					 }
				 
				 
				
				 if(itemsPrices.get(i)=="") {		
						ps.setFloat(5, 0);
						product_price_date.setPrice("0");
						 System.out.println(product_price_date.getIdtable() +" NO PRICE: "+ itemsPrices.get(i));
						 System.out.println(product);
					}else {
						ps.setFloat(5, Float.parseFloat(itemsPrices.get(i)));
						product_price_date.setPrice(itemsPrices.get(i));
					}
		
				ps.setString(6, product.getBrand()); 
				ps.setString(7, product.getCategory());
				ps.setString(8, product.getUrl()); 
				
			
				product_price_date.setId(product.getId());
				product_price_date.setTitle(product.getTitle());
		
				product_price_date.setBrand(product.getBrand());
				product_price_date.setCategory(product.getCategory());
				product_price_date.setUrl(product.getUrl());	
			 // System.out.println(product_price_date);
		
			 // TODO insertInCassandraProductPriceDate METODU ILE DOGRUDAN CASSANDRAYA EKLEME YAPIMCA BELLI BIR ZAMAN SONRA CASSANDRA DONUYOR. 
			 // insertInCassandraProductPriceDate(product_price_date);	
			}
			
			public int getBatchSize() {
				return itemsDates.size();
			}
			
		  });  
	}

	public void insertInRdbmsBatchProductPriceDate(){
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM product";	 
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		List<String> itemsPrices = null;
		List<String> itemsDates = null;	
		
		for (Map<?, ?> row : rows) {
			Product product = new Product();
			product.setId((Integer)row.get("id"));
			product.setPrices((String)row.get("prices"));
			product.setDates((String)row.get("dates"));
			product.setTitle((String)row.get("title"));
			product.setBrand((String)row.get("brand"));
			product.setCategory((String)row.get("category"));
			product.setUrl((String)row.get("url"));
																		// \s* any number of whitespace characters
			itemsPrices = Arrays.asList(((String) row.get("prices")).split("\\s*,\\s*"));
			itemsDates  = Arrays.asList(((String) row.get("dates")).split("\\s*,\\s*"));

			insertInRdbmsBatchPricesDates(product, itemsDates, itemsPrices);
		}
	}
	
	public void insertInRdbmsBatchSQL(final String sql){
		jdbcTemplate = new JdbcTemplate(dataSource); 
		jdbcTemplate.batchUpdate(new String[]{sql});
	}
}