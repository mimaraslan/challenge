package com.cimri.models;

public class Product {

	private Integer idtable;
	private Integer id;
	private String prices;
	private String dates;
	private String title;
	private String brand;
	private String category;
	private String url;

	public Product() {
	}

	public Product(Integer id, String prices, String dates, String title, String brand, String category, String url) {
		this.id = id;
		this.prices = prices;
		this.dates = dates;
		this.title = title;
		this.brand = brand;
		this.category = category;
		this.url = url;
	}

	public Product(Integer idtable, Integer id, String prices, String dates, String title, String brand,
			String category, String url) {
		this.idtable = idtable;
		this.id = id;
		this.prices = prices;
		this.dates = dates;
		this.title = title;
		this.brand = brand;
		this.category = category;
		this.url = url;
	}

	public Integer getIdtable() {
		return idtable;
	}

	public void setIdtable(Integer idtable) {
		this.idtable = idtable;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrices() {
		return prices;
	}

	public void setPrices(String prices) {
		this.prices = prices;
	}

	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Product [idtable=" + idtable + ", id=" + id + ", prices=" + prices + ", dates=" + dates + ", title="
				+ title + ", brand=" + brand + ", category=" + category + ", url=" + url + "]";
	}

}
