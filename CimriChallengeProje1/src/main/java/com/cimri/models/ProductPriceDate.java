package com.cimri.models;

public class ProductPriceDate {

	private Integer idtable;
	private Integer id;
	private String title;
	private String pricedate;
	private String calendardate;
	private String price;
	private String brand;
	private String category;
	private String url;

	public ProductPriceDate() {
	}

	public ProductPriceDate(Integer idtable, Integer id, String title, String pricedate, String calendardate, String price, String brand, String category, String url) {
		this.idtable = idtable;
		this.id = id;
		this.title = title;
		this.pricedate = pricedate;
		this.calendardate = calendardate;
		this.price = price;
		this.brand = brand;
		this.category = category;
		this.url = url;
	}

	public Integer getIdtable() {
		return idtable;
	}

	public void setIdtable(Integer idtable) {
		this.idtable = idtable;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPricedate() {
		return pricedate;
	}

	public void setPricedate(String pricedate) {
		this.pricedate = pricedate;
	}

	public String getCalendardate() {
		return calendardate;
	}

	public void setCalendardate(String calendardate) {
		this.calendardate = calendardate;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "ProductPriceDate [idtable=" + idtable + ", id=" + id + ", title=" + title + ", pricedate=" + pricedate
				+ ", calendardate=" + calendardate + ", price=" + price + ", brand=" + brand + ", category=" + category
				+ ", url=" + url + "]";
	}

}