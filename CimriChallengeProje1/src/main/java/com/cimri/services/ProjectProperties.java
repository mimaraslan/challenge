package com.cimri.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProjectProperties {
	String resultPropertiesValue = "";
	InputStream inputStream;
 
	public String getProperty(String propertiesName) throws IOException {
 
		try {
			Properties prop = new Properties();
			String propFileName = "application.properties";
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
	
			resultPropertiesValue= prop.getProperty(propertiesName);
		
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return resultPropertiesValue;
	}
}