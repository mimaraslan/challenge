package com.cimri.services;

import java.io.File;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ConfigurableApplicationContext;

public class ProductUtils {

	  public static boolean deleteDir(File dir) {
  	    if (dir.isDirectory()) {
  	      String[] children = dir.list();
  	      for (int i = 0; i < children.length; i++) {
  	        boolean success = deleteDir(new File(dir, children[i]));
  	        if (!success) {
  	          return false;
  	        }
  	      }
  	    }
  	    return dir.delete();
  	  }
	
	  
	  public static void createProductCsv(ConfigurableApplicationContext context) {

	   JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
	   Job jobProduct = (Job) context.getBean("productJob");

		try {
			JobExecution execution = jobLauncher.run(jobProduct, new JobParameters());
			System.out.println("jobProduct Exit Status : " + execution.getStatus());
		} catch (JobExecutionException e) {
			System.out.println("jobProduct failed");
			e.printStackTrace();
		}
	  }
	    
	  
	  public static void createProductPriceDateCsv(ConfigurableApplicationContext context) {

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");	
		Job jobProductPriceDate = (Job) context.getBean("productPriceDateJob");

		try {
			JobExecution execution = jobLauncher.run(jobProductPriceDate, new JobParameters());
			System.out.println("jobProductPriceDate Exit Status : " + execution.getStatus());
		} catch (JobExecutionException e) {
			System.out.println("jobProductPriceDate failed");
			e.printStackTrace();
		}
	  }

}
