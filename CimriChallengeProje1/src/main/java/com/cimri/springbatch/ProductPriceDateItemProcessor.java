package com.cimri.springbatch;

import org.springframework.batch.item.ItemProcessor;

import com.cimri.models.ProductPriceDate;

public class ProductPriceDateItemProcessor implements ItemProcessor<ProductPriceDate, ProductPriceDate>{

	public ProductPriceDate process(ProductPriceDate result) throws Exception {
		System.out.println("Processing result :"+result);
		
		if(result.getId() < 0){
			return null;
		}
		
		return result;
	}
}
