package com.cimri.springbatch;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.cimri.models.Product;

public class ProductRowMapper implements RowMapper<Product>{

	 public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		 Product result = new Product();
			result.setIdtable(rs.getInt("idtable"));
			result.setId(rs.getInt("id"));
			result.setPrices(rs.getString("prices"));
			result.setDates(rs.getString("dates"));
			result.setTitle(rs.getString("title"));
			result.setBrand(rs.getString("brand"));
			result.setCategory(rs.getString("category"));
			result.setUrl(rs.getString("url"));		
		 return result;
		} 
}
