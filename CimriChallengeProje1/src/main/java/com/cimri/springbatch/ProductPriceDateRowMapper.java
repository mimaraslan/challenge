package com.cimri.springbatch;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.cimri.models.ProductPriceDate;

public class ProductPriceDateRowMapper implements RowMapper<ProductPriceDate>{

	 public ProductPriceDate mapRow(ResultSet rs, int rowNum) throws SQLException {
		 ProductPriceDate result = new ProductPriceDate();		
			result.setIdtable(rs.getInt("idtable"));
			result.setId(rs.getInt("id"));
			result.setTitle(rs.getString("title"));
			result.setPricedate(rs.getString("pricedate"));
			result.setCalendardate(rs.getString("calendardate"));
			result.setPrice(rs.getString("price"));
			result.setBrand(rs.getString("brand"));
			result.setCategory(rs.getString("category"));
			result.setUrl(rs.getString("url"));
		 return result;
		} 
}
