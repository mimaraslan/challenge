package com.cimri.springbatch;

import org.springframework.batch.item.ItemProcessor;

import com.cimri.models.Product;

public class ProductItemProcessor implements ItemProcessor<Product, Product>{

	public Product process(Product result) throws Exception {
		System.out.println("Processing result :"+result);
		
		if(result.getId() < 0){
			return null;
		}
	
		return result;
	}
}
