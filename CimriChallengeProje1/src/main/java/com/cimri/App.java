package com.cimri;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.cimri.models.Product;
import com.cimri.repositories.ProductRepository;
import com.cimri.services.ProductUtils;
import com.cimri.services.ProjectProperties;

public class App {
	 
	public static void main(String[] args) throws IOException {
		Instant start = Instant.now();
       
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("context-application.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("resources/context-application.xml");

		ProductRepository productRepository = (ProductRepository) context.getBean("productRepository");

		try {

			productRepository.createInRdbmsDatabase();
			
			productRepository.createInCassandraDatabase();
			productRepository.createInCassandraTableProduct();
			productRepository.createInCassandraTableProductPriceDate();		
			
			productRepository.createInRdbmsTableProduct();
			productRepository.createInRdbmsTableProductTitle();
			productRepository.createInRdbmsTableProductTitleGroup();
			productRepository.createInRdbmsTableProductPriceDate();

			List<Product> products = new App().start();
			productRepository.insertInRdbmsBatchProduct(products);

			// PARAMETERE DEGERI EGER SIFIR SAYISI OLURSA SIRALAMA ASC, BASKA BIR SAYI OLURSA  DESC
			productRepository.sortInRdbmsTableProductOrderByTitle(0);
			productRepository.sortInRdbmsTableProductGroupByTitle();
			productRepository.sortInRdbmsTableProductPriceDate();

			productRepository.insertInRdbmsBatchProductPriceDate();
		
			ProductUtils.deleteDir(new File("data/product.csv"));
			ProductUtils.createProductCsv(context);
			
			ProductUtils.deleteDir(new File("data/product_price_date.csv"));
			ProductUtils.createProductPriceDateCsv(context);
			
			//TODO NORMALDE CVS DOSYALARINI JAVA KODUNDAN OTOMATIK OLARAK CASSANDRAYA EKLEYECEKTIM AMA BUNA IZIN VERMIYOR/
			//INTERNETTE YAPTIGIM ARASTIRMALAR NETICESINDE BU KISMI TERMINALDEN ELLE YAPINCA EKLENIYOR.
			//EKLEME KODU ASAGIDA CVS DOSYASININ KONUMUNU application.properties ICINDEN KENDI BILGISAYARINIZA GORE DEGISTIRMEYI UNUTMAYINIZ.		
			//productRepository.copyPoductCsvToCassandra();
			//productRepository.copyPoductPriceDateCsvToCassandra();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Instant end = Instant.now();
			System.out.println(start);
			System.out.println(end);
			Duration timeElapsed = Duration.between(start, end);
			System.out.println(timeElapsed.toMillis());
		}
		context.close();
	}

	private List<Product> start() throws Exception {

		List<Product> listFinalProducts = new ArrayList<Product>();

		//PROGRAM ICINDE DEGISEN ADRES GIBI BILGILERI DISARIDAN DEGISTIREBILMEK ICIN DAHA PRATIK HALE GETIRDIK.
		ProjectProperties projectProperties = new ProjectProperties();
	
		//URL DOSYALARININ KONUMLARINI application.properties ICINDEN KENDI BILGISAYARINIZA GORE DEGISTIRMEYI UNUTMAYINIZ.		
		//INTERNETTEN URLDEN CALISACAGIM ZAMAN BU ADRESLERDEN XML'LERI CEKERIYORUM.
	
	  URL site1_ = new URL(projectProperties.getProperty("url.site1.xml"));
	  URL site2_ = new URL(projectProperties.getProperty("url.site2.xml"));
	  URL site3_ = new URL(projectProperties.getProperty("url.site3.xml"));
	  URL site4_ = new URL(projectProperties.getProperty("url.site4.xml"));

		listFinalProducts.addAll(getFromXMLProducts(site1_));
		listFinalProducts.addAll(getFromXMLProducts(site2_));
		listFinalProducts.addAll(getFromXMLProducts(site3_));
		listFinalProducts.addAll(getFromXMLProducts(site4_));
	
		
		//DAHA HIZLI CALISMAK ICIN DOSYALARI KENDI LOCAL BILGISAYRIMA INDIRDIM BURADAN CEKTIM DAHA HIZLI OLUYOR VE INTERNETIMI TUKETMIYOR.
		//URL DOSYALARININ KONUMLARINI application.properties ICINDEN KENDI BILGISAYARINIZA GORE DEGISTIRMEYI UNUTMAYINIZ.		
		//URL site1 = new URL(projectProperties.getProperty("file.site1.xml"));
		//URL site2 = new URL(projectProperties.getProperty("file.site2.xml"));
		//URL site3 = new URL(projectProperties.getProperty("file.site3.xml"));
		//URL site4 = new URL(projectProperties.getProperty("file.site4.xml"));
		
		//listFinalProducts.addAll(getFromXMLProducts(site1));
		//listFinalProducts.addAll(getFromXMLProducts(site2));
		//listFinalProducts.addAll(getFromXMLProducts(site3));
		//listFinalProducts.addAll(getFromXMLProducts(site4));
		
		//URL site5 = new URL(projectProperties.getProperty("file.site5.xml"));
		//listFinalProducts.addAll(getFromXMLProducts(site5));
		
		return listFinalProducts;
	}

	private List<Product> getFromXMLProducts(URL urlXml) throws Exception {
		URLConnection connection = urlXml.openConnection();
		Document doc = parseXML(connection.getInputStream());
		/*
		 NodeList descNodes = doc.getElementsByTagName("row"); 
		 for (int i = 0; i < descNodes.getLength(); i++) {
		 	System.out.println(descNodes.item(i).getTextContent());
		 }
		 */
		NodeList nList = doc.getElementsByTagName("row");

		List<Product> products = new ArrayList<Product>();

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			// System.out.println("\nSIRA: "+temp);
			// System.out.println("Current Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
	      
				String id = eElement.getElementsByTagName("id").item(0).getTextContent();
				String prices = eElement.getElementsByTagName("prices").item(0).getTextContent();
				String dates = eElement.getElementsByTagName("dates").item(0).getTextContent();
				String title = eElement.getElementsByTagName("title").item(0).getTextContent();
				String brand = eElement.getElementsByTagName("brand").item(0).getTextContent();
				String category = eElement.getElementsByTagName("category").item(0).getTextContent();
				String url = eElement.getElementsByTagName("url").item(0).getTextContent();
				/*
				 System.out.println("Row id : " + id); 
				 System.out.println("prices : " + prices); 
				 System.out.println("dates : " + dates);
				 System.out.println("title : " + title); 
				 System.out.println("brand : " + brand); 
				 System.out.println("category : " + category);
				 System.out.println("url : " + url);
				 */
				Product productNew = new Product(Integer.valueOf(id), prices, dates, title, brand, category, url);
				products.add(productNew);
			}
		}
		return products;
	}

	private Document parseXML(InputStream stream) throws Exception {
		DocumentBuilderFactory objDocumentBuilderFactory = null;
		DocumentBuilder objDocumentBuilder = null;
		Document doc = null;
		
		try {
			objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
			objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

			doc = objDocumentBuilder.parse(stream);
		} catch (Exception ex) {
			throw ex;
		}
		
		return doc;
	}

}